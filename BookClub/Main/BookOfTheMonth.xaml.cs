﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BookClub
{
    /// <summary>
    /// Interaction logic for BookOfTheMonth.xaml
    /// </summary>
    /// Named button SubmitFeedBackButton
    /// Textbox = FeedbackTextBox
    public partial class BookOfTheMonth : Window
    {
        public BookOfTheMonth()
        {
            InitializeComponent();
            BookOfTheMonthStorage store = new BookOfTheMonthStorage();
            List<BookMonth> book = store.Read();
            bookList.ItemsSource = book;
        }

        private void SubmitFeedbackButton_Click(object sender, RoutedEventArgs e)
        {

            Feedback feed = new Feedback()
            {
                review = FeedbackTextBox.Text,
                
            };

            FeedbackStorage store = new FeedbackStorage();
            store.Save(feed);

            FeedbackTextBox.Clear();
            
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void ViewFeedback_Click(object sender, RoutedEventArgs e)
        {
            FeedbackViewer win = new FeedbackViewer();
            win.Closed += Win_Closed;
            win.Show();
            this.Hide();
        }
        

        private void AllBookNav_Click(object sender, RoutedEventArgs e)
        {
            AllBooks win = new AllBooks();
            win.Closed += Win_Closed;
            win.Show();
            this.Hide();
        }

        private void Win_Closed(object sender, EventArgs e)
        {
            this.Show();
        }
    }
}
