﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookClub
{
    class BookOfTheMonthStorage
    {
        private string file = "bookofthemonth.json";
        public void Save(BookMonth book)
        {
            List<BookMonth> addBookMonth = Read();
            addBookMonth.Add(book);
            string json = JsonConvert.SerializeObject(addBookMonth);
            File.WriteAllText(file, json);
        }
        public List<BookMonth> Read()
        {
            if (!File.Exists(file))
            {
                return new List<BookMonth>();
            }
            string all = File.ReadAllText(file);
            List<BookMonth> results = JsonConvert.DeserializeObject<List<BookMonth>>(all);
            return results;
        }
        public BookMonth FindItem(BookMonth exampleBook)
        {
            List<BookMonth> results = Read();
            BookMonth foundBook = new BookMonth();
            foreach (BookMonth thisBook in results)
            {
                if (thisBook.Title == exampleBook.Title && thisBook.Author == exampleBook.Author)
                {
                    foundBook = thisBook;
                }
            }
            return foundBook;
        }
        //public BookMonth RemoveItem(BookMonth selectedbook)
    }
}
