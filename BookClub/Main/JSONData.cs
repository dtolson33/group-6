﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BookClub.Main
{
    class JSONData
    {
        public DataGetSet ConvertToObject(string data)
        {
            JObject rawbook = JObject.Parse(data);
            List<JToken> token = rawbook["data"].Children().ToList();
            JToken first = token.First();

            DataGetSet myBook = first.ToObject<DataGetSet>();
            return myBook;
        }
    }
}
