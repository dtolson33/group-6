﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookClub
{
    class IsbnChecker
    {
        public bool ValidIsbn10(string isbn10)
        {
            bool valid = false;
            if(!string.IsNullOrEmpty(isbn10))
            {
                if(isbn10.Contains("-"))
                {
                    isbn10 = isbn10.Replace("-", "");
                }
                long x;

                if(isbn10.Length != 10 || !long.TryParse(isbn10.Substring(0 , isbn10.Length - 1) , out x))
                {
                    return false;
                }
                char finalDigit = isbn10[isbn10.Length - 1];
                int sum = 0;
                for (int i = 0; i < 9; i++)
                {
                    sum += int.Parse(isbn10[i].ToString()) * (i + 1);
                }
                int remainder = sum % 11;
                //Checking to see if the check digit is X
                if (finalDigit == 'X')
                {
                    valid = (remainder == 10);
                }
                //Otherwise treating the last digit as a number
                else if (int.TryParse(finalDigit.ToString(), out sum))
                {
                    valid = (remainder == int.Parse(finalDigit.ToString()));
                }
                 
            }
            return valid;
        }




        public bool ValidIsbn13(string isbn13)
        {
            bool valid = false;
            if (!string.IsNullOrEmpty(isbn13))
            {
                if (isbn13.Contains("-"))
                {
                    isbn13 = isbn13.Replace("-", "");
                }
                long x;

                if (isbn13.Length != 13 || !long.TryParse(isbn13, out x))
                {
                    return false;
                }
                int sum = 0;
                for (int i = 0; i < 12; i++)
                {
                    sum += int.Parse(isbn13[i].ToString()) * (i % 2 == 1 ? 3 : 1);
                }
                int remainder = sum % 10;
                int checkDigit = 10 - remainder;

                if(checkDigit == 10)
                {
                    checkDigit = 0;
                }

                valid = (checkDigit == int.Parse(isbn13[12].ToString()));

            }
            return valid;
        }


    }
}
