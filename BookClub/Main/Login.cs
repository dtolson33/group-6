﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookClub.Main
{
    public delegate void LoginEventHandler(string loginName, Boolean status);

    class Login_Data
    {
        public event LoginEventHandler LoginEvent;
        
        public void Login(string loginName, string password)
        {
            if (loginName == "jeremy" && password == "admin")
            {
                LoginEvent(loginName, true);
            }
            else if (loginName == "david" && password == "admin")
            {
                LoginEvent(loginName, true);
            }
            else 
            {
                LoginEvent(loginName, false);
            }
        } 
    }
}
