﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookClub
{
    class AllBookStorage
    {
        private string file = "allbooks.json";
        public void Save(Book book)
        {
            List<Book> addBook = Read();
            addBook.Add(book);
            string json = JsonConvert.SerializeObject(addBook);
            File.WriteAllText(file, json);
            
        }
        public List<Book> Read()
        {
            if (!File.Exists(file))
            {
                return new List<Book>();
            }
            string all = File.ReadAllText(file);
            List<Book> results = JsonConvert.DeserializeObject<List<Book>>(all);
            return results;
        }
        

        public Book FindItem(Book exampleBook)
        {
            List<Book> results = Read();
            Book foundBook = new Book();
            foreach (Book thisBook in results)
            {
                if (thisBook.Title == exampleBook.Title && thisBook.Author == exampleBook.Author)
                {
                    foundBook = thisBook;
                }
            }
            return foundBook;
        }
    }
}
