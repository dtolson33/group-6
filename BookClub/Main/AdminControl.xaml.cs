﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BookClub
{
    /// <summary>
    /// Interaction logic for AdminControl.xaml
    /// </summary>
    public partial class AdminControl : Window
    {
        public AdminControl()
        {
            InitializeComponent();
        }

        private void AllBooksButton_Click(object sender, RoutedEventArgs e)
        {
            AllBooks win = new AllBooks();
            win.Closed += Win_Closed;
            win.Show();
            this.Hide();
        }

        private void BookoftheMonthButton_Click(object sender, RoutedEventArgs e)
        {
            BookOfTheMonth win = new BookOfTheMonth();
            win.Closed += Win_Closed;
            win.Show();
            this.Hide();
        }
        

        private void button_Click(object sender, RoutedEventArgs e)
        {
            AddBook win = new AddBook();
            win.Closed += Win_Closed;
            win.Show();
            this.Hide();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            AddBookOfTheMonth win = new AddBookOfTheMonth();
            win.Closed += Win_Closed;
            win.Show();
            this.Hide();
        }
        private void Win_Closed(object sender, EventArgs e)
        {
            this.Show();
        }

        private void withoutISBN_Click(object sender, RoutedEventArgs e)
        {
            AddBookAPI win = new AddBookAPI();
            win.Closed += Win_Closed;
            win.Show();
            this.Hide();
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            EditBook win = new EditBook();
            win.Closed += Win_Closed;
            win.Show();
            this.Hide();
        }

        private void removeButton_Click(object sender, RoutedEventArgs e)
        {
            RemoveBook win = new RemoveBook();
            win.Closed += Win_Closed;
            win.Show();
            this.Hide();
        }
    }
}
