﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookClub
{
    public delegate void LoginEventHandler(string loginName, bool status);

    class LoginInfo
    {
        public event LoginEventHandler LoginEvent;

        public void AdminLogin(string loginName, string password)
        {
            if (loginName == "jhansen" && password == "0123")
            {
                LoginEvent(loginName, true);
            }
            else if (loginName == "dolson" && password == "4567")
            {
                LoginEvent(loginName, true);
            }
            else
            {
                LoginEvent(loginName, false);
            }
        }
        public void UserLogin(string loginName, string password)
        {
            if (loginName == "user" && password == "user")
            {
                LoginEvent(loginName, true);
            }
            else
            {
                LoginEvent(loginName, false);
            }
        }
    }
}
