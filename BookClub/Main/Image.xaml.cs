﻿using System.Windows;
using System.Windows.Forms;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace BookClub.Main
{
    
    public partial class Image : Window
    {
        Image File;

        public Image()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();
            f.Filter = "JPG(*.JPG)|*.jpg";

            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                File = Image.Fromile(f.FileName);
                image.Image = File;

            }
        }

    }
}
