﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookClub.Main
{
    class DataGetSet
    {
        private string title
        {
            get;
            set;
        }
        private string author_data
        {
            get;
            set;
        }
        private int isbn10
        {
            get;
            set;
        }
        private int isbn13
        {
            get;
            set;
        }
        private string summary
        {
            get;
            set;
        }
        private string price
        {
            get;
            set;
        }
    }

}
