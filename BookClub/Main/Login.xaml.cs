﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BookClub
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }
        private void buttonLogin_Click(object sender, RoutedEventArgs e)
        {
            if ((textUsername.Text).Contains("user"))
            {
                LoginInfo newUserLogin = new LoginInfo();
                newUserLogin.LoginEvent += new LoginEventHandler(newUserLogin_LoginEvent);
                newUserLogin.UserLogin(textUsername.Text, textPassword.Text);
                BookOfTheMonth win = new BookOfTheMonth();
                win.Closed += Win_Closed;
                win.Show();
                this.Hide();
            }
            else
            {
                LoginInfo newAdminLogin = new LoginInfo();
                newAdminLogin.LoginEvent += new LoginEventHandler(newAdminLogin_LoginEvent);
                newAdminLogin.AdminLogin(textUsername.Text, textPassword.Text);
                AdminControl win = new AdminControl();
                win.Closed += Win_Closed;
                win.Show();
                this.Hide();
            }
        }
        void newUserLogin_LoginEvent(string loginName, bool status)
        {
            if(status == true)
            {
                MessageBox.Show("Welcome to Jeremey and David's Book Club!");
            }
            
        }
        void newAdminLogin_LoginEvent(string loginName, bool status)
        {
            if (status == true)
            {
                MessageBox.Show("Welcome!");
            }
        }
        private void Win_Closed(object sender, EventArgs e)
        {
            this.Show();
        }
    }
}
