﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BookClub
{
    /// <summary>
    /// Interaction logic for AddBook.xaml
    /// </summary>
    public partial class AddBook : Window
    {
        public AddBook()
        {
            InitializeComponent();
        }
        //Creating book object and giving it to a list

        Book book = new Book();
        List<Book> BookLog = new List<Book>();
        IsbnChecker isbn = new IsbnChecker();

        private void IsbnButton_Click(object sender, RoutedEventArgs e)
        {

            //Check to see how long the string entered into the text box is
            //Depending on the size either send it to the isbn10 or isbn13 checker
            //Make button to submit only valid if the check validity has been passed as true
            string checker = IsbnTextBox.Text;

            if (!string.IsNullOrEmpty(checker)) //Checking if there is something in the text box
            {
                if (checker.Contains("-")) //Replacing any dashes to find the true size of the string
                {
                    checker = checker.Replace("-", "");
                }
                if (checker.Length == 10)
                {
                    if (isbn.ValidIsbn10(checker) == true)
                    {
                        System.Windows.MessageBox.Show("Valid ISBN 10");
                        SubmitButton.IsEnabled = true; 
                    }
                    if (isbn.ValidIsbn10(checker) == false)
                    {
                        System.Windows.MessageBox.Show("Please enter valid ISBN");
                    }
                }
                if (checker.Length == 13)
                {
                    if (isbn.ValidIsbn13(checker) == true)
                    {
                        System.Windows.MessageBox.Show("Valid ISBN 13");
                        SubmitButton.IsEnabled = true; //Not sure this will work
                    }
                    if (isbn.ValidIsbn13(checker) == false)
                    {
                        System.Windows.MessageBox.Show("Please enter valid ISBN");
                    }
                }
                //if (IsbnTextBox.Text = "none", "None") 
                //Need to be able to pass an book through if it doesn't have an ISBN
                else
                {
                    System.Windows.MessageBox.Show("Please enter either an ISBN-10 or an ISBN-13");
                }
            }

        }
        
        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            Book book = new Book()
            {
                Title = TitleTextBox.Text,
                Author = AuthorTextBox.Text,
                Price = Convert.ToDecimal(PriceTextBox.Text),
                Description = DescriptionTextBox.Text,
                Isbn = IsbnTextBox.Text
            };

            AllBookStorage store = new AllBookStorage();
            store.Save(book);
           
            TitleTextBox.Clear();
            AuthorTextBox.Clear();
            PriceTextBox.Clear();
            DescriptionTextBox.Clear();
            IsbnTextBox.Clear();
        }
    }
}
