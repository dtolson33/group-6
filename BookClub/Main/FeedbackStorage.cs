﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookClub
{
    class FeedbackStorage
    {
        private string file = "feedback.json";
        public void Save(Feedback f)
        {
            List<Feedback> addFeedback = Read();
            addFeedback.Add(f);
            string json = JsonConvert.SerializeObject(addFeedback);
            File.WriteAllText(file, json);
        }
        public List<Feedback> Read()
        {
            if (!File.Exists(file))
            {
                return new List<Feedback>();
            }
            string all = File.ReadAllText(file);
            List<Feedback> results = JsonConvert.DeserializeObject<List<Feedback>>(all);
            return results;
        }
    }
}
