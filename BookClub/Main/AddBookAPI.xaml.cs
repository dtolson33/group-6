﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.Net;
using BookClub.Main;
using System.IO;

namespace BookClub
{
    /// <summary>
    /// Interaction logic for AddBookAPI.xaml
    /// </summary>
    public partial class AddBookAPI : Window
    {
        public AddBookAPI()
        {
            InitializeComponent();
        }
        private string key = "8L391B9Q";


        private void buttonAddBook_Click(object sender, RoutedEventArgs e)
        {

            string input = textTitleInput.Text;
            WebClient wc = new WebClient();
            JSONData j = new JSONData();
            string uri = string.Format(@"http://isbndb.com/api/v2/json/{0}/book/{1}", key, input);
            string data = wc.DownloadString(uri);
            //Need to export data into David's Json format in the book class, steal this from the AddBook
            // it is the Book book = new Book()
            textBox.Text = data;
            j.ConvertToObject(data);
        }

        private static J json_data<J>(string url) where J : new()
        {
            using (var w = new WebClient())
            {
                var json_data = string.Empty;
                // attempt to download JSON data as a string
                try
                {
                    json_data = w.DownloadString(url);
                }
                catch (Exception) { }
                // if string with JSON data is not empty, deserialize it to class and return its instance 
                return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<J>(json_data) : new J();

            }
        }
    }
}
